Install
=============

Import file Excel to database
------

```
npm run import
```

Start Server
--------

```
npm start
```

Test Eslint
---------

```
npm run lint

```

Test Mocha 
--------

```
npm test
```

Test nyc
---------

```
npm run test:nyc
```

Coverage nyc to html 

```
npm run coverage
```